package com.alinabon.b142.s01.helloworld;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class HelloWorldApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelloWorldApplication.class, args);
	}

	@GetMapping("api/hello")
	public String hello(){
		return String.format("Hello, World! Welcome to Java + SpringBoot.");
	}

	@GetMapping("api/hello-name")
	public String helloName(@RequestParam(value = "name", defaultValue = "World") String name) {
		return String.format("Hello, %s! Welcome to Java + Spring Boot.", name);
	}

	@GetMapping("api/your-name/{name}")
	public String yourName(@PathVariable("name") String name){
		return String.format("Hello %S!, How are you doing today", name);
	}

	@GetMapping("api/good-evening")
	public String goodEvening(){
		return String.format("Good evening!");
	}

	@GetMapping("hi")
	public String hi(@RequestParam(value = "user", defaultValue = "user") String user){
		return String.format("hi %s", user);
	}
}



